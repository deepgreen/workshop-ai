# Workshop-AI

### Materials for the 2019-11-07 UVM DeepGreen Workshop.

### Installation

To clone this repo from a jupyter notebook type:
    `!git clone https://gitlab.uvm.edu/deepgreen/workshop-ai.git workshopai`
    
You should now have all workshop materials and you can open the intro python notebook.

### Support
The creation of these materials was supported in part by the National Science Foundation under Award No. OAC-1827314.

[<img width="100px" src="https://www.nsf.gov/images/logos/NSF_4-Color_bitmap_Logo.png">](https://www.nsf.gov/awardsearch/showAward?AWD_ID=1827314)
